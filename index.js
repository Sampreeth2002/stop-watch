var hours = 0;
var minutes = 0;
var second = 0;
var freeze = 0;

var hourHeading = document.getElementById("hours");
var mintuesHeading = document.getElementById("mintues");
var secondHeading = document.getElementById("second");
var lapTime = document.getElementById("lap");
var interval = null;
lapTime.textContent = "";

function timer() {
  second++;
  if (second < 10) {
    secondHeading.innerHTML = "0" + second;
  } else {
    secondHeading.innerHTML = second;
  }
  if (second >= 60) {
    minutes++;
    if (minutes < 10) {
      mintuesHeading.innerHTML = "0" + minutes;
    } else {
      mintuesHeading.innerHTML = minutes;
    }
    second = 0;
  } else if (minutes >= 60) {
    hours++;
    hourHeading.innerHTML = hours;
    minutes = 0;
  }
}

function start() {
  if (interval == null) {
    interval = setInterval(timer, 1000);
  }
}

function stop() {
  clearInterval(interval);
  interval = null;
}

function reset() {
  stop();
  hours = 0;
  minutes = 0;
  second = 0;
  secondHeading.innerHTML = "00";
  mintuesHeading.innerHTML = "00";
  hourHeading.innerHTML = "00";
  lapTime.textContent = "";
}

function lap() {
  freeze =
    hourHeading.textContent +
    " : " +
    mintuesHeading.textContent +
    " : " +
    secondHeading.textContent;
  lapTime.innerHTML += "<li>" + freeze + "</li>";
}
